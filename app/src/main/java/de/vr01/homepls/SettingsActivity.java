package de.vr01.homepls;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SettingsActivity extends AppCompatActivity {

    public static final String SETTINGSFILE = "Settings";
    EditText et_own_address, et_own_city, et_buddyname_buddy1,
            et_buddyname_buddy2,et_buddyname_buddy3,et_buddyname_buddy4,
            et_buddynumber_buddy1,et_buddynumber_buddy2,et_buddynumber_buddy3,
            et_buddynumber_buddy4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        et_own_address = (EditText) findViewById(R.id.et_own_address);
        et_own_city = (EditText) findViewById(R.id.et_own_city);
        et_buddyname_buddy1 = (EditText) findViewById(R.id.et_buddy1_name);
        et_buddyname_buddy2 = (EditText) findViewById(R.id.et_buddy2_name);
        et_buddyname_buddy3 = (EditText) findViewById(R.id.et_buddy3_name);
        et_buddyname_buddy4 = (EditText) findViewById(R.id.et_buddy4_name);
        et_buddynumber_buddy1 = (EditText) findViewById(R.id.et_buddy1_phone);
        et_buddynumber_buddy2 = (EditText) findViewById(R.id.et_buddy2_phone);
        et_buddynumber_buddy3 = (EditText) findViewById(R.id.et_buddy3_phone);
        et_buddynumber_buddy4 = (EditText) findViewById(R.id.et_buddy4_phone);
        final Button save = (Button) findViewById(R.id.button_save);

        loadData();

        save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent nextScreen = new Intent(getApplicationContext(), MainActivity.class);
                saveData();
                startActivity(nextScreen);

            }
        });
    }

    private void saveData(){
        SharedPreferences.Editor editor = getSharedPreferences(SETTINGSFILE, MODE_PRIVATE).edit();
        editor.putString("homeAddress", et_own_address.getText().toString());
        editor.putString("homeCity", et_own_city.getText().toString());
        editor.putString("buddyNameBuddy1", et_buddyname_buddy1.getText().toString());
        editor.putString("buddyNumberBuddy1",et_buddynumber_buddy1.getText().toString());
        editor.putString("buddyNameBuddy2", et_buddyname_buddy2.getText().toString());
        editor.putString("buddyNumberBuddy2", et_buddynumber_buddy2.getText().toString());
        editor.putString("buddyNameBuddy3", et_buddyname_buddy3.getText().toString());
        editor.putString("buddyNumberBuddy3", et_buddynumber_buddy3.getText().toString());
        editor.putString("buddyNameBuddy4", et_buddyname_buddy4.getText().toString());
        editor.putString("buddyNumberBuddy4", et_buddynumber_buddy4.getText().toString());
        editor.apply();
    }
    private void loadData(){
        SharedPreferences prefs = getSharedPreferences(SETTINGSFILE, MODE_PRIVATE);
        et_own_address.setText(prefs.getString("homeAddress", ""), TextView.BufferType.EDITABLE);
        et_own_city.setText(prefs.getString("homeCity", ""), TextView.BufferType.EDITABLE);
        et_buddyname_buddy1.setText(prefs.getString("buddyNameBuddy1", ""), TextView.BufferType.EDITABLE);
        et_buddyname_buddy2.setText(prefs.getString("buddyNameBuddy2", ""), TextView.BufferType.EDITABLE);
        et_buddyname_buddy3.setText(prefs.getString("buddyNameBuddy3", ""), TextView.BufferType.EDITABLE);
        et_buddyname_buddy4.setText(prefs.getString("buddyNameBuddy4", ""), TextView.BufferType.EDITABLE);

        et_buddynumber_buddy1.setText(prefs.getString("buddyNumberBuddy1", ""), TextView.BufferType.EDITABLE);
        et_buddynumber_buddy2.setText(prefs.getString("buddyNumberBuddy2", ""), TextView.BufferType.EDITABLE);
        et_buddynumber_buddy3.setText(prefs.getString("buddyNumberBuddy3", ""), TextView.BufferType.EDITABLE);
        et_buddynumber_buddy4.setText(prefs.getString("buddyNumberBuddy4", ""), TextView.BufferType.EDITABLE);

    }
}
