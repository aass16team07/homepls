package de.vr01.homepls;

import java.util.ArrayList;

/**
 * Created by Peta on 16.07.2016.
 */
public class DirectionsRoute {
    private ArrayList<DirectionsSegment> route;

    public DirectionsRoute(){
        route = new ArrayList<>();
    }


    public void extendRoute(DirectionsSegment segment)
    {
        route.add(segment);
    }

    public ArrayList<DirectionsSegment> getRoute() {
        return route;
    }
}
