package de.vr01.homepls;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.percent.PercentRelativeLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class BuddyActivity extends AppCompatActivity {

    public static final String SETTINGSFILE = "Settings";
    TextView name1, name2, name3, name4, location;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buddy);
        final SharedPreferences prefs = getSharedPreferences(SETTINGSFILE, MODE_PRIVATE);
        Typeface fontPrimary = Typeface.createFromAsset(getAssets(), "Brandon_bld_0.otf");


        name1 = (TextView) findViewById(R.id.buddy_tv_buddyname_buddy1);
        name1.setTypeface(fontPrimary);
        name2 = (TextView) findViewById(R.id.buddy_tv_buddyname_buddy2);
        name2.setTypeface(fontPrimary);
        name3 = (TextView) findViewById(R.id.buddy_tv_buddyname_buddy3);
        name3.setTypeface(fontPrimary);
        name4 = (TextView) findViewById(R.id.buddy_tv_buddyname_buddy4);
        name4.setTypeface(fontPrimary);
        location = (TextView) findViewById(R.id.textview_standort_addresse);
        location.setTypeface(fontPrimary);
        location.setText(prefs.getString("locatedAddress", ""));



        name1.setText(prefs.getString("buddyNameBuddy1", ""));
        name2.setText(prefs.getString("buddyNameBuddy2", ""));
        name3.setText(prefs.getString("buddyNameBuddy3", ""));
        name4.setText(prefs.getString("buddyNameBuddy4", ""));

        final String lol = prefs.getString("buddyNumberBuddy1", "");
        final String lol2 = prefs.getString("buddyNumberBuddy1", "");
        final String lol3 = prefs.getString("buddyNumberBuddy1", "");
        final String lol4 = prefs.getString("buddyNumberBuddy1", "");

        ImageView iv_call_buddy1 = (ImageView) findViewById(R.id.buddy_iv_buddycall_buddy1);
        iv_call_buddy1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                call(lol);
            }
        });
        ImageView iv_call_buddy2 = (ImageView) findViewById(R.id.buddy_iv_buddycall_buddy2);
        iv_call_buddy1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                call(lol2);
            }
        });
        ImageView iv_call_buddy3 = (ImageView) findViewById(R.id.buddy_iv_buddycall_buddy3);
        iv_call_buddy1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                call(lol3);
            }
        });
        ImageView iv_call_buddy4 = (ImageView) findViewById(R.id.buddy_iv_buddycall_buddy4);
        iv_call_buddy1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                call(lol4);
            }
        });

        ImageView iv_whatsapp_buddy1 = (ImageView) findViewById(R.id.buddy_iv_buddywhatsapp_buddy1);
        iv_whatsapp_buddy1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sendLocation(prefs);

            }
        });
    }

    protected void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    private void sendLocation(SharedPreferences prefs){
        boolean installed = appInstalledOrNot("com.whatsapp");
        if(installed) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            final String locatedAddress = prefs.getString("locatedAddress", "");
            StringBuilder whatsappMessage = new StringBuilder();
            whatsappMessage.append(getString(R.string.buddy_whatsapp_preamble) + " " + locatedAddress);
            sendIntent.putExtra(Intent.EXTRA_TEXT, whatsappMessage.toString());
            sendIntent.setType("text/plain");
            sendIntent.setPackage("com.whatsapp");
            startActivity(sendIntent);
        }
        else
        {
            showToast(getString(R.string.whatsapp_not_installed));
        }
    }
    private void call(String number) {
        if (number.length() > 0) {
            StringBuilder callString = new StringBuilder();
            callString.append("tel:");
            callString.append(number);

            PackageManager pm = getPackageManager();
            if (pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY)) {
                Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(callString.toString()));
                startActivity(callIntent);
            }
            else{
                showToast(getString(R.string.device_cannot_call));
            }
        }
        else
        {
            showToast(getString(R.string.number_not_specified));
        }
    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

}
