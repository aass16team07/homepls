package de.vr01.homepls;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;


public class OeffiActivity extends AppCompatActivity {

    private DirectionsRoute route;
    private LinearLayout ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oeffi);
        ll = (LinearLayout)findViewById(R.id.LinearLayout);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String value = extras.getString("jsonOeffi");
            try {
                getOeffi(value);
                drawList();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void getOeffi(String response) throws JSONException {
        JSONObject object = (JSONObject) new JSONTokener(response).nextValue();
        JSONArray array = object.getJSONArray("routes");
        JSONObject routes = array.getJSONObject(0);
        JSONArray legs = routes.getJSONArray("legs");
        JSONObject leg = legs.getJSONObject(0);
        JSONArray steps = leg.getJSONArray("steps");

        route = new DirectionsRoute();

        for (int i=0; i < steps.length(); i++)
        {
            JSONObject step = steps.getJSONObject(i);
            if(step.getString("travel_mode").contains("TRANSIT"))
            {
                DirectionsSegment segment = new DirectionsSegment();
                segment.setArrivalStop(step.getJSONObject("transit_details").getJSONObject("arrival_stop").getString("name"));
                Log.d("JSONtest",step.getJSONObject("transit_details").getJSONObject("arrival_stop").getString("name"));
                segment.setArrivalTime(step.getJSONObject("transit_details").getJSONObject("arrival_time").getString("text"));
                segment.setDepartureStop(step.getJSONObject("transit_details").getJSONObject("departure_stop").getString("name"));
                segment.setDepartureTime(step.getJSONObject("transit_details").getJSONObject("departure_time").getString("text"));
                segment.setLineShort(step.getJSONObject("transit_details").getJSONObject("line").getString("short_name"));
                segment.setHeadsign(step.getJSONObject("transit_details").getString("headsign"));
                segment.setVehicleType(step.getJSONObject("transit_details").getJSONObject("line").getJSONObject("vehicle").getString("name"));
                segment.setStops(step.getJSONObject("transit_details").getString("num_stops"));
                route.extendRoute(segment);
            }
        }
    }

    private void drawList(){

        for (DirectionsSegment temp : route.getRoute()) {

            final TextView departureStop = new TextView(this);
            final TextView arrivalStop = new TextView(this);
            final TextView line = new TextView(this);
            final TextView spacer = new TextView(this);

            departureStop.setText(temp.getDepartureTime() + " " + temp.getDepartureStop());
            departureStop.setBackgroundResource(R.drawable.oeffi_departure);
            departureStop.setTextColor(Color.parseColor("#FFFFFF"));

            line.setText("» " + temp.getVehicleType() + " " + temp.getLineShort() + " nach " + temp.getHeadsign() + " [" + temp.getStops() + " Stationen]");
            line.setBackgroundResource(R.drawable.oeffi_direction);

            arrivalStop.setText(temp.getArrivalTime() + " " + temp.getArrivalStop());
            arrivalStop.setBackgroundResource(R.drawable.oeffi_arrival);

            spacer.setHeight(20);

            ll.addView(spacer);
            ll.addView(departureStop);
            ll.addView(line);
            ll.addView(arrivalStop);

        }

    }
}
