package de.vr01.homepls;

import java.util.ArrayList;

/**
 * Created by Peta on 16.07.2016.
 */
public class DirectionsSegment {

    private String departureStop;
    private String departureTime;
    private String arrivalStop;
    private String arrivalTime;
    private String headsign;
    private String lineShort;
    private String vehicleType;
    private String stops;


    public DirectionsSegment() {
        this.departureStop = "";
        this.departureTime = "";
        this.arrivalStop = "";
        this.arrivalTime = "";
        this.headsign = "";
        this.lineShort = "";
        this.vehicleType = "";
        this.stops = "";
    }

    public String getDepartureStop() {
        return departureStop;
    }

    public void setDepartureStop(String departureStop) {
        this.departureStop = departureStop;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getArrivalStop() {
        return arrivalStop;
    }

    public void setArrivalStop(String arrivalStop) {
        this.arrivalStop = arrivalStop;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getHeadsign() {
        return headsign;
    }

    public void setHeadsign(String headsign) {
        this.headsign = headsign;
    }

    public String getLineShort() {
        return lineShort;
    }

    public void setLineShort(String lineShort) {
        this.lineShort = lineShort;
    }

    public String getStops() {
        return stops;
    }

    public void setStops(String stops) {
        this.stops = stops;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }
}
