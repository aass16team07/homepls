package de.vr01.homepls;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.percent.PercentRelativeLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.graphics.Typeface;
import android.graphics.Color;
import android.widget.Toast;

import java.text.DecimalFormat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.concurrent.ExecutionException;


public class MainActivity extends AppCompatActivity implements LocationAssistant.Listener {

    /**
     * Name der Auslagerungsdatei.
     */
    public static final String SETTINGSFILE = "Settings";
    public static final String API_KEY = "AIzaSyCMToHYLXExje9U64Kr7ZomcqWWU7ATuDE";

    private static final int TEXTVIEWCOUNT = 14;
    private LocationAssistant assistant;
    private TextView textview_standort_addresse;
    ImageView reloadbutton;

    TextView textview_oeffi_time, textview_oeffi_price, textview_oeffi_title,
            textview_taxi_time, textview_taxi_price, textview_taxi_title,
            textview_buddy_time, textview_buddy_price, textview_buddy_title,
            textview_laufen_time, textview_laufen_price, textview_laufen_title,
            textview_standort_title;

    String jsonOeffi, jsonTaxi, jsonBuddy, jsonLaufen;


    private Location mLastLocation;

    protected static final String ADDRESS_REQUESTED_KEY = "address-request-pending";
    protected static final String LOCATION_ADDRESS_KEY = "location-address";
    private AddressResultReceiver mResultReceiver;
    private String mAddressOutput, homeAddress, mAddressOutputFocus;
    private boolean homeAddressSet = false, mAddressSet = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textview_standort_addresse = (TextView) findViewById(R.id.textview_standort_addresse);
        textview_standort_addresse.setText(getString(R.string.empty));

        assistant = new LocationAssistant(this, this, LocationAssistant.Accuracy.HIGH, 8000, true);
        mResultReceiver = new AddressResultReceiver(new Handler());
        mAddressOutputFocus = "";

        genText();
        loadData();

        try {
            if (checkResult(getGeocoderJSON(homeAddress)))
                homeAddressSet = true;
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        final Button settings = (Button) findViewById(R.id.button_settings);
        settings.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent nextScreen = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(nextScreen);
            }
        });

        reloadbutton = (ImageView) findViewById(R.id.imageview_reloadicon);
        reloadbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (mAddressSet && homeAddressSet && isOnline()) {
                    mAddressOutputFocus = mAddressOutput;
                    showToast(getString(R.string.address_set));
                    try {

                        qryRoutes();
                        saveData();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (!mAddressSet)
                        showToast(getString(R.string.no_location_data_provided));
                    if (!homeAddressSet)
                        showToast(getString(R.string.home_not_found));
                    if(!isOnline())
                        showToast(getString(R.string.offline));
                }
            }
        });

        PercentRelativeLayout rl = (PercentRelativeLayout) findViewById(R.id.box_oeffi);
        rl.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (jsonOeffi != null) {
                    Intent i = new Intent(getApplicationContext(), OeffiActivity.class);
                    i.putExtra("jsonOeffi", jsonOeffi);
                    startActivity(i);
                }
            }
        });
        PercentRelativeLayout rl2 = (PercentRelativeLayout) findViewById(R.id.box_taxi);
        rl2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                PackageManager pm = getPackageManager();
                if (pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY)) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:030202020"));
                                        startActivity(callIntent);
                }
                else {
                    showToast(getString(R.string.device_cannot_call));
                }
            }
        });
        PercentRelativeLayout rl3 = (PercentRelativeLayout)findViewById(R.id.box_buddy);
        rl3.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                if(jsonOeffi != null) {
                    Intent i = new Intent(getApplicationContext(), BuddyActivity.class);
                    startActivity(i);
                }
            }
        });
        PercentRelativeLayout rl4 = (PercentRelativeLayout)findViewById(R.id.box_laufen);
        rl4.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                if(jsonLaufen != null) {
                    Intent i = new Intent(getApplicationContext(), LaufenActivity.class);
                    i.putExtra("jsonLaufen", jsonLaufen);
                    startActivity(i);
                }
            }
        });
    }


    private void genText()
    {
        textview_oeffi_time = (TextView) findViewById(R.id.textview_oeffi_time);
        textview_oeffi_price = (TextView) findViewById(R.id.textview_oeffi_price);
        textview_oeffi_title = (TextView) findViewById(R.id.textview_oeffi_title);
        textview_taxi_time = (TextView) findViewById(R.id.textview_taxi_time);
        textview_taxi_price = (TextView) findViewById(R.id.textview_taxi_price);
        textview_taxi_title = (TextView) findViewById(R.id.textview_taxi_title);
        textview_buddy_time = (TextView) findViewById(R.id.textview_buddy_time);
        textview_buddy_price = (TextView) findViewById(R.id.textview_buddy_price);
        textview_buddy_title = (TextView) findViewById(R.id.textview_buddy_title);
        textview_laufen_time = (TextView) findViewById(R.id.textview_laufen_time);
        textview_laufen_price = (TextView) findViewById(R.id.textview_laufen_price);
        textview_laufen_title = (TextView) findViewById(R.id.textview_laufen_title);
        textview_standort_title = (TextView) findViewById(R.id.textview_standort_title);
        textview_standort_addresse = (TextView) findViewById(R.id.textview_standort_addresse);
        TextView[] allTextViews = new TextView[TEXTVIEWCOUNT];

        Typeface fontPrimary = Typeface.createFromAsset(getAssets(), "Brandon_blk.otf");

        textview_oeffi_time.setTextColor(Color.parseColor("#FFFFFF"));
        allTextViews[0] = textview_oeffi_time;
        allTextViews[1] = textview_oeffi_price;
        textview_oeffi_title.setAllCaps(true);
        allTextViews[2] = textview_oeffi_title;
        textview_taxi_time.setTextColor(Color.parseColor("#FFFFFF"));
        allTextViews[3] = textview_taxi_time;
        allTextViews[4] = textview_taxi_price;
        textview_taxi_title.setAllCaps(true);
        allTextViews[5] = textview_taxi_title;
        textview_buddy_time.setTextColor(Color.parseColor("#FFFFFF"));
        allTextViews[6] = textview_buddy_time;
        allTextViews[7] = textview_buddy_price;
        textview_buddy_title.setAllCaps(true);
        allTextViews[8] = textview_buddy_title;
        textview_laufen_time.setTextColor(Color.parseColor("#FFFFFF"));
        allTextViews[9] = textview_laufen_time;
        allTextViews[10] = textview_laufen_price;
        textview_laufen_title.setAllCaps(true);
        allTextViews[11] = textview_laufen_title;
        textview_standort_title.setTextColor(Color.parseColor("#5682b2"));
        allTextViews[12] = textview_laufen_time;
        textview_standort_addresse.setTextColor(Color.parseColor("#FFFFFF"));
        allTextViews[13] = textview_standort_addresse;
        textview_laufen_title.setAllCaps(true);

        for(int i=0; i<TEXTVIEWCOUNT; i++)
        {
            allTextViews[i].setTypeface(fontPrimary);
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
    private void loadData(){
        SharedPreferences prefs = getSharedPreferences(SETTINGSFILE, MODE_PRIVATE);
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(prefs.getString("homeAddress",""));
        strBuilder.append(" ");
        strBuilder.append(prefs.getString("homeCity",""));
        homeAddress = strBuilder.toString();
        textview_oeffi_time.setText(prefs.getString("oeffiTime",""));
        textview_taxi_time.setText(prefs.getString("taxiTime",""));
        textview_buddy_time.setText(prefs.getString("buddyTime",""));
        textview_laufen_time.setText(prefs.getString("laufenTime",""));

    }

    private void saveData(){
        SharedPreferences.Editor editor = getSharedPreferences(SETTINGSFILE, MODE_PRIVATE).edit();
        editor.putString("oeffiTime", textview_oeffi_time.getText().toString());
        editor.putString("taxiTime", textview_taxi_time.getText().toString());
        editor.putString("buddyTime", textview_buddy_time.getText().toString());
        editor.putString("laufenTime", textview_laufen_time.getText().toString());
        editor.putString("locatedAddress", mAddressOutputFocus);
        editor.commit();

    }

    private void qryRoutes() throws ExecutionException, InterruptedException, JSONException {
        reloadbutton.setEnabled(false);
        String result = getDistanceJSON(mAddressOutputFocus, homeAddress, "transit");
        if(checkResult(result)) {
            jsonOeffi = result;
            getTimeOeffi(result);
        }
        else {
            showToast(getString(R.string.route_not_found));
        }
        result = getDistanceJSON(mAddressOutputFocus, homeAddress, "walking");
        if(checkResult(result)) {
            jsonLaufen = result;
            getTimeLaufen(result);
        }
        else {
            showToast(getString(R.string.route_not_found));
        }
        result = getDistanceJSON(mAddressOutputFocus, homeAddress, "driving");
        if(checkResult(result)) {
            getTimeFahren(result);
        }
        else {
            showToast(getString(R.string.route_not_found));
        }
        reloadbutton.setEnabled(true);
    }
    private String getDistanceJSON(String origin, String destination, String mode) throws ExecutionException, InterruptedException {
        String originCleaned = origin.replace(" ", "+").replace("\n", "+").replace("\r", "+");
        String destinationCleaned = destination.replace(" ", "+").replace("\n", "+").replace("\r", "+");
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/directions/json?");
        urlString.append("origin=");//from
        urlString.append(originCleaned);
        urlString.append("&destination=");//to
        urlString.append(destinationCleaned);
        urlString.append("&mode=" + mode);
        if(mode.contains("transit"))
            urlString.append("&transit_routing_preference=less_walking");
        urlString.append("&language=de-DE");
        urlString.append("&key=" + API_KEY);

        return (new RetrieveURL().execute(urlString.toString()).get());
      }

    private String getGeocoderJSON(String address) throws ExecutionException, InterruptedException {
        String addressCleaned = address.replace(" ", "+").replace("\n", "+").replace("\r", "+");
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/geocode/json?");
        urlString.append("address=");
        urlString.append(addressCleaned);
        urlString.append("&language=de-DE");
        urlString.append("&key=" + API_KEY);
        return(new RetrieveURL().execute(urlString.toString()).get());
    }
    private void getTimeOeffi(String response) throws JSONException {
        JSONObject object = (JSONObject) new JSONTokener(response).nextValue();
        JSONArray array = object.getJSONArray("routes");
        JSONObject routes = array.getJSONObject(0);
        JSONArray legs = routes.getJSONArray("legs");
        JSONObject steps = legs.getJSONObject(0);
        JSONObject duration = steps.getJSONObject("duration");
        textview_oeffi_time.setText(calcTime(duration.getInt("value")));

    }
    private void getTimeLaufen(String response) throws JSONException {
        JSONObject object = (JSONObject) new JSONTokener(response).nextValue();
        JSONArray array = object.getJSONArray("routes");
        JSONObject routes = array.getJSONObject(0);
        JSONArray legs = routes.getJSONArray("legs");
        JSONObject steps = legs.getJSONObject(0);
        JSONObject duration = steps.getJSONObject("duration");
        textview_laufen_time.setText(calcTime(duration.getInt("value")));

    }
    private void getTimeFahren(String response) throws JSONException {
        JSONObject object = (JSONObject) new JSONTokener(response).nextValue();
        JSONArray array = object.getJSONArray("routes");
        JSONObject routes = array.getJSONObject(0);
        JSONArray legs = routes.getJSONArray("legs");
        JSONObject steps = legs.getJSONObject(0);
        JSONObject duration = steps.getJSONObject("duration");
        JSONObject distance = steps.getJSONObject("distance");
        textview_taxi_time.setText(calcTime(duration.getInt("value")));

        textview_taxi_price.setText(calcPrice(distance.getInt("value")));
    }

    boolean checkResult(String result) throws JSONException {
            JSONObject object = (JSONObject) new JSONTokener(result).nextValue();
            return (object.getString("status").compareTo("OK") == 0 ? true : false);
     }

    private String calcPrice(int distance)
    {
        StringBuilder priceString = new StringBuilder();
        DecimalFormat df = new DecimalFormat("0.00");
        float prcFloat = (float) (((distance*2)/1000)+ 3.90);
        priceString.append(getString(R.string.approx)+" ");
        priceString.append(df.format(prcFloat));
        priceString.append(" " + getString(R.string.currency));

        return priceString.toString();
    }
    private String calcTime(int duration){
        StringBuilder durationString = new StringBuilder();
        if ((int) duration/60 > 90)
        {
            durationString.append(duration/60/60 + "h" + duration/60%60 + "min");
        }
        else{
            durationString.append((int)duration/60);
            durationString.append(" min");
        }
        return durationString.toString();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        assistant.start();
    }

    @Override
    protected void onPause()
    {
        assistant.stop();
        super.onPause();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        textview_standort_addresse.setOnClickListener(null);
        assistant.onPermissionsUpdated();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        assistant.onActivityResult(requestCode, resultCode);
    }

    @Override
    public void onNeedLocationPermission()
    {
        textview_standort_addresse.setText("Need\nPermission");
        assistant.requestAndPossiblyExplainLocationPermission();
    }

    @Override
    public void onExplainLocationPermission()
    {
        new AlertDialog.Builder(this)
                .setMessage(R.string.permissionExplanation)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        assistant.requestLocationPermission();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        textview_standort_addresse.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                assistant.requestLocationPermission();
                            }
                        });
                    }
                })
                .show();
    }

    @Override
    public void onNeedLocationSettingsChange()
    {
        new AlertDialog.Builder(this)
                .setMessage(R.string.switchOnLocationShort)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        assistant.changeLocationSettings();
                    }
                })
                .show();
    }

    @Override
    public void onFallBackToSystemSettings(View.OnClickListener fromView, DialogInterface.OnClickListener fromDialog) {
        new AlertDialog.Builder(this)
                .setMessage(R.string.switchOnLocationLong)
                .setPositiveButton(R.string.ok, fromDialog)
                .show();
    }

    @Override
    public void onNewLocationAvailable(Location location) {
        if (location == null) return;
        mLastLocation = location;
        startIntentService();
        textview_standort_addresse.setOnClickListener(null);
        //textview_standort_addresse.setText(location.getLongitude() + "\n" + location.getLatitude());
        textview_standort_addresse.setAlpha(1.0f);
        textview_standort_addresse.animate().alpha(0.8f).setDuration(400);
    }

    @Override
    public void onMockLocationsDetected(View.OnClickListener fromView, DialogInterface.OnClickListener fromDialog) {
        textview_standort_addresse.setText(getString(R.string.mockLocationMessage));
        textview_standort_addresse.setOnClickListener(fromView);
    }

    @Override
    public void onError(LocationAssistant.ErrorType type, String message) {
        showToast(getString(R.string.error));
    }


    /**
     *  Create an intent for passing to the intent service responsible for fetching the address.
     */
    protected void startIntentService() {

        Intent intent = new Intent(this, FetchAddressIntentService.class);
        intent.putExtra(Constants.RECEIVER, mResultReceiver);
        intent.putExtra(Constants.LOCATION_DATA_EXTRA, mLastLocation);
        startService(intent);
    }


    protected void displayAddressOutput() {
        textview_standort_addresse.setText(mAddressOutput);
    }
    protected void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    /**
     *  Empfängt Daten vom FetchAddressIntentService und Updatet die UI in MainActivity.
     */
    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            mAddressOutput = resultData.getString(Constants.RESULT_DATA_KEY);
            displayAddressOutput();
            if (resultCode == Constants.SUCCESS_RESULT) {
                mAddressSet = true;
            }
            else
            {
                mAddressSet = false;
            }

        }
    }


}
