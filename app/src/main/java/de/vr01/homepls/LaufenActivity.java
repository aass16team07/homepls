package de.vr01.homepls;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class LaufenActivity extends AppCompatActivity {
    private DirectionsRoute route;
    private LinearLayout ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laufen);

        ll = (LinearLayout)findViewById(R.id.LinearLayout);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String value = extras.getString("jsonLaufen");
            try {
                getLaufen(value);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    private void getLaufen(String response) throws JSONException {
        JSONObject object = (JSONObject) new JSONTokener(response).nextValue();
        JSONArray array = object.getJSONArray("routes");
        JSONObject routes = array.getJSONObject(0);
        JSONArray legs = routes.getJSONArray("legs");
        JSONObject leg = legs.getJSONObject(0);
        JSONArray steps = leg.getJSONArray("steps");

        route = new DirectionsRoute();

        for (int i=0; i < steps.length(); i++)
        {
            final TextView instruction = new TextView(this);
            final TextView distance = new TextView(this);
            final TextView duration = new TextView(this);
            final TextView spacer = new TextView(this);


            JSONObject step = steps.getJSONObject(i);
            instruction.setText(Html.fromHtml(step.getString("html_instructions")));
            instruction.setBackgroundResource(R.drawable.oeffi_departure);
            instruction.setTextColor(Color.parseColor("#FFFFFF"));

            distance.setText(step.getJSONObject("distance").getString("text"));
            distance.setBackgroundResource(R.drawable.oeffi_direction);
            ll.addView(instruction);
            ll.addView(distance);


        }
    }

}
