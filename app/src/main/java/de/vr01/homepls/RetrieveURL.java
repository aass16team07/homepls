package de.vr01.homepls;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

class RetrieveURL extends AsyncTask<String, Void, String> {

    private Exception exception;


    protected String doInBackground(String... params) {
        try {

            // get the JSON And parse it to get the directions data.
            HttpURLConnection urlConnection= null;
            URL url = null;
            Log.d("xxx","URL2=" + params[0]);
            url = new URL(params[0]);
            urlConnection=(HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            urlConnection.connect();

            InputStream inStream = urlConnection.getInputStream();
            BufferedReader bReader = new BufferedReader(new InputStreamReader(inStream));

            String temp, response = "";
            while((temp = bReader.readLine()) != null){
                //Parse data
                response += temp;
            }
            Log.d("xxx","Return="+response);
            //Close the reader, stream & connection
            bReader.close();
            inStream.close();
            urlConnection.disconnect();

            return response;
        } catch (Exception e) {
            this.exception = e;
            return null;
        }
    }


    @Override
    protected void onPostExecute(String result) {

    }

}